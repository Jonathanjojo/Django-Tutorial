from django.shortcuts import render, redirect
from .models import Article, Like
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from . import forms

def article_list(request):
    articles = Article.objects.all().order_by('date')
    return render(request, 'articles/article_list.html', {'articles' : articles})

def article_detail(request, slug):
    article = Article.objects.get(slug=slug)
    return render(request, 'articles/article_detail.html', {'article' : article})

@login_required(login_url="/accounts/login")
def article_create(request):
    if request.method == "POST":
        form = forms.CreateArticle(request.POST, request.FILES)
        if form.is_valid():
            # Get the author, but not saving it just yet
            instance = form.save(commit=False)
            instance.author = request.user
            # Now save it for real
            instance.save()
            return redirect('articles:list')
    else:
        form = forms.CreateArticle()
    return render(request, 'articles/article_create.html', {'form':form})

@login_required(login_url="/accounts/login")
def article_comment(request, slug):
    article = Article.objects.get(slug=slug)
    if request.method == "POST":
        form = forms.CreateComment(request.POST)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.author = request.user
            instance.article = article
            instance.save()
            return redirect('articles:detail', slug=slug)
    else:
        form = forms.CreateComment()
    return render(request, 'articles/article_comment_create.html', {'form': form, 'article':article})

@login_required(login_url="/accounts/login")
def article_like(request, slug):
    article = Article.objects.get(slug=slug)
    if request.method == "POST":
        new_like, created = Like.objects.get_or_create(author=request.user, article=article)
        if created:
            new_like.save()   
            return redirect('articles:detail', slug=slug)
    return redirect(request.META['HTTP_REFERER'])