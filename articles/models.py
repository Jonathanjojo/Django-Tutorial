from django.db import models
from django.contrib.auth.models import User

class Article(models.Model):
    title = models.CharField(max_length=100)
    slug = models.SlugField(max_length=100)
    body = models.TextField()
    date = models.DateTimeField(auto_now_add=True)
    thumbnail = models.ImageField(default='default.png', blank=True)
    author = models.ForeignKey(User, default=None, on_delete=models.CASCADE)

    def __str__(self):
        return self.title

    def snippet(self):
        if len(self.body) > 100:
            return self.body[:100] + " ..."
        else:
            return self.body

class Comment(models.Model):
    article = models.ForeignKey('articles.Article', on_delete=models.CASCADE, related_name='comments')
    text = models.CharField(max_length=100)
    date = models.DateTimeField(auto_now_add=True)
    author = models.ForeignKey(User, default=None, on_delete=models.CASCADE)

    def __str__(self):
        return self.text
    
class Like(models.Model):
    author = models.ForeignKey(User, default=None, on_delete=models.CASCADE)    
    article = models.ForeignKey('articles.Article', on_delete=models.CASCADE, related_name='likes')
    date = models.DateTimeField(auto_now_add=True)