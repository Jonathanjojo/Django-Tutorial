# Django Tutorial

## What is Django?

Django (/ˈdʒæŋɡoʊ/ jang-goh) is a free and open source web application framework, written in Python. A web framework is a set of components that helps you to develop websites faster and easier.

When you're building a website, you always need a similar set of components: a way to handle user authentication (signing up, signing in, signing out), a management panel for your website, forms, a way to upload files, etc.

Luckily for you, other people long ago noticed that web developers face similar problems when building a new site, so they teamed up and created frameworks (Django being one of them) that give you ready-made components to use.

Frameworks exist to save you from having to reinvent the wheel and to help alleviate some of the overhead when you’re building a new site.

## Prerequisites
Please prepare these requirements before our meet up according to your Operating Systems (different OS might need different procedures)

1. Python 3.6.* (preferably the latest)
2. pip 18.0 (usually comes with python)
3. virtualenv 16.0.* (preferably the latest, install it with pip)
4. Text Editor (recommended: Sublime, Atom, VSCode, etc)
5. Browser App 
6. Git Account (gitlab.com / github.com) and git CLI on your local machine

## Downloading and Installing this Repo (*skip it if you want to go through the tutorial*)

Get into your chosen directory and fire up your terminal. In the terminal, run these commands and follow the instructions on the terminal if exists :

```shell
git clone https://gitlab.com/Jonathanjojo/Django-Tutorial.git
virtualenv env
source env/bin/activate
pip install -r requirements.txt
python manage.py createsuperuser
python manage.py makemigrations
python manage.py migrate
python manage.py runserver
```

## Installing Django

Lets get into your chosen directory (could be anywhere), create a new directory that is **tutorial** and fire up your text editor and command terminal.

*Note: these commands run in Ubuntu/Debian OS, some may differ for Windows OS, your mentors would guide you through it :)*

1. Initialize git repository
    * Create a new git project from respected websites (gitlab.com or github.com)
    * Copy the project url
    * On your terminal (inside tutorial/ directory) :  
    
    ```shell
    git init
    git config --global user.name "MY_USER_NAME"
    git config --global user.email "MY_EMAIL_ADDRESS"
    git remote add origin [copy your project url here]
    git remote -v                                           ==> make sure you got it right (push and fetch)
    git add .
    git commit -m "Initial Commit"
    git push origin master
    ```


2. **Make a virtual-env and start it**

    ```shell
    python -m venv env
    source env/bin/activate
    ```
    *env* is the name of our virtual-env

3. **Create requirements.txt**
    
    Make **requirements.txt** and write:

    ```txt
    Django
    ```

4. **Installing Requirement : Django**

    ```shell
    pip install -r requirements.txt
    ```

5. **Start a django project**

    ```shell
    django-admin startproject mysite
    ```
    *mysite* is the name of our project

    By now, your directory should look like this:

    ```bash
    tutorial
    ├───env
    │       ...
    ├───mysite
    │       settings.py
    │       urls.py
    │       wsgi.py
    │       __init__.py
    ├───.git
    ├───db.sqlite3
    ├───manage.py
    └───requirements.txt
    ```

    Check if it is working by running the server and see the outcome on your browser

    ```shell
    python manage.py runserver
    ```

    And then, on your browser, open up   **localhost:8000**
    If you get this on your browser, congratulations! You have successfully started a Django project!

    ![Django-Success](https://cdn-images-1.medium.com/max/1600/1*XtgQ1xPyydYGsf4jDvCX0A.png)


## Creating your first Django Project

For now, pay attention to your Mentors for further information :)

### Article App

#### Creating the App
* Create new app called "Article"

    ```shell
    python manage.py startapp articles
    ```
* Add the new app to mysite/settings.py
    On the list of INSTALLED_APPS, add 'articles' on the very end.

#### Urls
Include articles' url in the mysite's url, on mysite/urls.py

```python
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('articles/', include('articles.urls')),
]
```

#### View/Template
Make a view template for the article app

* Make new directory inside articles

    ```shell
    mkdir templates
    cd templates
    mkdir articles
    cd articles
    ```

* Your directory should be like 
    ```bash
    tutorial
    ├───articles
    │       __pycache__
    │       migrations
    │       templates
    │           articles
    │               article_list.html
    │       __init__.py
    │       admin.py
    │       apps.py
    │       models.py
    │       tests.py
    │       urls.py
    │       views.py
    ├───env
    ├───mysite
    ├───.git
    ├───db.sqlite3
    ├───manage.py
    └───requirements.txt
    ```

* (Cont.) On articles/templates/articles, create article_list.html, containing: 

    ```html
    <!DOCTYPE html>
    <html>

    <head>
        <meta charset="utf-8">
        <title>Articles</title>
    </head>

    <body>
        <h1>Article List</h1>
    </body>

    </html>
    ```

* Change articles/views.py

    ```python
    from django.shortcuts import render

    def article_list(request):
        return render(request, 'articles/article_list.html')
    ```
    *articles* here refers to templates/articles/

#### Models

On articles/models.py, we add an Article model (class) :

```python
from django.db import models

class Article(models.Model):
    title = models.CharField(max_length=100)
    slug = models.SlugField(max_length=100)
    body = models.TextField()
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title
```

#### Migrations

The model we made is just in our codes, the database hasn't known about it yet, and so the table has not been map yet according to our model. We need to migrate it

* Make a migration file according to the model we just made

```shell
python manage.py makemigrations
```

You can see the change you made in the migrations folder. Then you have to migrate it to apply the change to the database

```shell
python manage.py migrate 
```

Do these 2 steps **everytime** you make a change or make a new model/migration

#### Django ORM (Object-Relational-Mapping)

It is used to interact with the database, you can make changes to the data on the database via the console/shell provided

```shell
python manage.py shell
```

One of the most used usage of this console is to populate/seed the database, basically filling the database with data. Try this on the console:

```python 
from articles.models import Article

article = Article()
article.title = "article title"
article.body = "article body"
article.save()
```

Use Article.objects.all() to retrive all article objects, add [index] to retrieve specific object. You can even retrive the attribute using [Object].attr

#### Django Admin

* Create a super user (admin)  

    ```shell
    python manage.py createsuperuser
    ```
* Fill the fields of username and password (+confirmation)

    Mine was :

    | Username      | Email         | Password  |
    | ------------- |---------------| ----------|
    | jonathanjojo  | jojo@jojo.com | justpass  |

* Run the server and go to localhost:8000/admin (as in the mysite/urls.py)
* Log in
* You can populate your database from here too, but first you have to register the model

On articles/admin.py, write:

```python
from django.contrib import admin
from .models import Article    

admin.site.register(Article)
```

#### Finishing up the Article List

On articles/views.py :

```python
from django.shortcuts import render
from .models import Article

def article_list(request):
    articles = Article.objects.all().order_by('date')
    return render(request, 'articles/article_list.html', {'articles' : articles})
```
The dictionary states the name of the variable passed to the html  
And then on articles/templates/articles/article_list.html, change it into:

```html
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Articles</title>
</head>

<body>
    <h1>Article List</h1>
    <div class="articles">
        {% for article in articles %}
            <div class="article">
                <h2><a href="#">{{ article.title }}</a></h2>
                <p>{{ article.body }}</p>     
                <p>{{ article.date }}</p>     
            </div>
        {% endfor %}
    </div>
</body>

</html>
```
*Note*  
* {%   %}  is for python codes  
* {{   }}  is for data

#### Snipping (Aesthetic)

Add snipping function to our article Model class, in case the body is longer than 100 characters

```python
def snippet(self):
        if len(self.body) > 100:
            return self.body[:100] + " ..."
        else:
            return self.body
```

And change the template as well. Change article.body into article.snippet

### Static Files

Inside the mysite/urls.py, add :

Basically we extend our urlpatterns with the static_file_urls.  
The STATIC_URL could be seen in our settings.py, it would appear that the url was '/static/  
It will be used from the user end, so the user would type the link [app]/static/[static-file]

For us, developers, we might add on our settings.py:

```python
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'assets')
)
```

And on tutorial/urls.py:
```python 
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('articles/', include('articles.urls')),
]

urlpatterns += staticfiles_urlpatterns()
```

Lets say we have this cool styles.css underneath newly made folder assets and we want to use it on our views html. 

```css
body {
    background-color: #0f121f;
    background-image: url(stars.png);
    background-repeat: no-repeat;
    color: #fff;
}

body *{
    font-family: Ubuntu;
}

h1, h2, h3, h4, h5 {
    font-weight: normal;
    margin: 0;
}

a, a:hover, a:visited {
    color: #fff;
    text-decoration: none;
}
```

We can update our article_list.html by adding a css link:

```html
{% load static from staticfiles %}

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Articles</title>
    <link rel="stylesheet" href="{% static 'styles.css' %}">
</head>

<body>
    <h1>Article List</h1>
    <div class="articles">
        {% for article in articles %}
            <div class="article">
                <h2><a href="#">{{ article.title }}</a></h2>
                <p>{{ article.snippet }}</p>     
                <p>{{ article.date }}</p>     
            </div>
        {% endfor %}
    </div>
</body>

</html>
```

#### Extending Base Template

* Make a template folder on root, so by now, your directory should look like this:

```bash
    tutorial
    ├───articles
    ├───assets
    ├───env
    ├───mysite
    ├───templates
    ├───db.sqlite3
    ├───manage.py
    └───requirements.txt
```

* Inside the template folder you just made, make a base_layout.html, containing the *common* component from article/template, here it goes

```html
{% load static from staticfiles %}

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Articles</title>
    <link rel="stylesheet" href="{% static 'styles.css' %}">
</head>

<body>
    <div class="wrapper">
        {% block content %}
        {% endblock %}
    </div>
</body>

</html>
```

Why did we do that? Well, now we can delete those *common* component of the template and replace it with just simple lines of code, here is how our article_list.html now looks like:

```html
{% extends 'base_layout.html' %}

{% block content%}
    <h1>Article List</h1>
    <div class="articles">
        {% for article in articles %}
            <div class="article">
                <h2><a href="#">{{ article.title }}</a></h2>
                <p>{{ article.snippet }}</p>     
                <p>{{ article.date }}</p>     
            </div>
        {% endfor %}
    </div>
{% endblock %}
```
Before that could work, we need to tell Django where we store our templates, so check out settings.py.  
In the list of TEMPLATES, heads to the key 'DIRS' and fill it with ['templates']. You are now good to go.

#### URL Parameter

We want it to be :
    if we click an article on the list, we will be redirected to the article detail by using the url articles/{id}. You may remember the slug we made on the model. We will use it as our url.

Now we will change our articles/urls.py to :

```python
from django.urls import path
from . import views

urlpatterns = [
    path('', views.article_list),
    path('<slug>/', views.article_detail)
]
```

And our articles/views.py temporary to:
```python
from django.shortcuts import render
from .models import Article
from django.http import HttpResponse

def article_list(request):
    articles = Article.objects.all().order_by('date')
    return render(request, 'articles/article_list.html', {'articles' : articles})

def article_detail(request, slug):
    return HttpResponse(slug)
```

#### Named URL

* Now change our articles/urls.py to:

    ```python
    from django.urls import path
    from . import views

    app_name = "article"

    urlpatterns = [
        path('', views.article_list, name="list"),
        path('<slug>/', views.article_detail, name="detail")
    ]
    ```
    Note the app_name, its purpose is to specify the url as you might see below


* Because we named our url, we no longer need to state the hardcoded url on our html, so on templates/base_layout.html, change it into: (might as well add a new image for fun, put in inside the assets folder)

    ```html
    {% load static from staticfiles %}

    <!DOCTYPE html>
    <html>

    <head>
        <meta charset="utf-8">
        <title>Articles</title>
        <link rel="stylesheet" href="{% static 'styles.css' %}">
    </head>

    <body>
        <div class="wrapper">
            <h1><a href="{% url 'article:list' %}"><img src="{% static 'logo.png' %}" alt="logo" height="42" width="42"/></a></h1>
            {% block content %}
            {% endblock %}
        </div>
    </body>

    </html>
    ```

And change the url on article_list.html as well:

    ```html
    {% extends 'base_layout.html' %}

    {% block content%}
        <h1>Article List</h1>
        <div class="articles">
            {% for article in articles %}
                <div class="article">
                    <h2><a href="{% url 'article:detail' slug=article.slug %}">{{ article.title }}</a></h2>
                    <p>{{ article.snippet }}</p>     
                    <p>{{ article.date }}</p>     
                </div>
            {% endfor %}
        </div>
    {% endblock %}
    ```

*Note*: Make sure all of your articles have their own slugs

#### Article Detail Template

Remember our temporary change on the article/views.py? Now we get it on again to:
```python
from django.shortcuts import render
from .models import Article
from django.http import HttpResponse

def article_list(request):
    articles = Article.objects.all().order_by('date')
    return render(request, 'articles/article_list.html', {'articles' : articles})

def article_detail(request, slug):
    article = Article.objects.get(slug=slug)
    return render(request, 'articles/article_detail.html', {'articles' : articles})
```

And now we can make our article_detail.html inside articles' template, just like article_list.html

```html
{% extends 'base_layout.html' %}

{% block content%}
<div class="article_detail">
    <div class="article">
        <h2>{{ article.title }}</h2>
        <p>{{ article.body }}</p>
        <p>{{ article.date }}</p>
    </div>
</div>

{% endblock %}
```

#### Uploading Media

* Add another variable on settings.py:  

    ```python
    MEDIA_URL = '/media/'

    MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

    # this is where Django would store the images
    ```

* On tutorial/urls.py:  
    ```python
    from django.conf.urls.static import static
    from django.conf import settings
    from django.contrib import admin
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns
    from django.urls import path, include

    urlpatterns = [
        path('admin/', admin.site.urls),
        path('articles/', include('articles.urls')),
    ]

    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    ```

* Now we want another field on Article model, which is thumbnail, add it to the class

    ```python
    thumbnail = models.ImageField(default='default.png', blank=True)
    ```
    Add the default.png on our media folder later. The folder would be created automatically after you updated one or more query in the database (make use of the superuser/admin page)

* Remember, now we need to migrate it again as so:

    ```shell
    python manage.py makemigrations && python manage.py migrate
    ```

* Now we shall update our article_detail.html, add another line of code to show the thumbnail
    ```html
    <img src="{{ article.thumbnail.url }}"/>
    ```

### Accounts App

#### Creating the App
* Create new app called "Accounts"

    ```shell
    python manage.py startapp accounts
    ```
* Add the new app to mysite/settings.py
    On the list of INSTALLED_APPS, add 'accounts' on the very end.

#### Create some URLs

Write in accounts/urls.py:

```python
from django.urls import path
from . import views

app_name = "accounts"

urlpatterns = [
    path('signup/', views.signup_view, name="signup"),
]
```

Next, include the url to mysite/urls.py with adding this into the urlpatterns

```python
path('accounts/', include('accounts.urls')),
```

Then we make the view accordingly, on accounts/views.py:  

```python
from django.shortcuts import render
from django.contrib.auth.forms import UserCreationForm

def signup_view(request):
    form = UserCreationForm()
    return render(request, 'accounts/signup.html', {'form':form})

```

Next, we shall make the template, so make a new folder named 'templates' and then a new folder within named 'accounts' just like we did on the articles app.  

Your directory should look like this now: 

```bash
tutorial
    ├───accounts
    │       __pycache__
    │       migrations
    │       templates
    │           accounts
    │               signup.html
    │       __init__.py
    │       admin.py
    │       apps.py
    │       models.py
    │       tests.py
    │       urls.py
    │       views.py
    ├───articles
    ├───env
    ├───mysite
    ├───.git
    ├───db.sqlite3
    ├───manage.py
    └───requirements.txt
```

Inside the last folder you made, create a new html file named signup.html as follows:

```html
{% extends 'base_layout.html' %}

{% block content%}
    <h1>Sign Up</h1>
    <form class="site-form" action="/accounts/signup/" method="POST">
        {{ form  }}
        <input type="submit" value="Sign Up">
    </form>
{% endblock %}
```

#### Saving User (Sign Up)

Since a signing up user would send a POST request (the user is sending data), we can update our views.py to:  

```python
from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm

def signup_view(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            # log the user in (cont.)
            return redirect('articles:list')
    else:
        form = UserCreationForm()
    return render(request, 'accounts/signup.html', {'form':form})

```

If you would run the app now, it would throw an error because we haven't csrf-tokenized the form (security purpose: so other website cannot send requests to our website without permission). So, we need to change a little bit of our signup.html

```html
{% extends 'base_layout.html' %}

{% block content%}
    <h1>Sign Up</h1>
    <form class="site-form" action="{% url 'accounts:signup' %}" method="POST">
        {% csrf_token %}
        {{ form  }}
        <input type="submit" value="Sign Up">
    </form>
{% endblock %}
```

You might try to sign up now and it will create a new user. I used floppy as a name, and everybodydotheflop as the password here.

#### Log In

* Add a new url to our accounts/urls.py:

    ```python
    path('login/', views.login_view, name="login"),
    ```
* Add a new view as well:  

    ```python
    from django.shortcuts import render, redirect
    from django.contrib.auth.forms import UserCreationForm, AuthenticationForm

    def signup_view(request):
        if request.method == "POST":
            form = UserCreationForm(request.POST)
            if form.is_valid():
                form.save()
                # log the user in (cont.)
                return redirect('articles:list')
        else:
            form = UserCreationForm()
        return render(request, 'accounts/signup.html', {'form':form})

    def login_view(request):
        if request.method =="POST":
            form = AuthenticationForm(data=request.POST)
            if form.is_valid():
                return redirect('articles:list')
                # log the user in (cont.)
        else:
            form = AuthenticationForm()
        return render(request, 'accounts/login.html', {'form':form})
    ```

* Add a new html template too:  

    ```html
    {% extends 'base_layout.html' %}

    {% block content%}
        <h1>Log In</h1>
        <form class="site-form" action="{% url 'accounts:login' %}" method="POST">
            {% csrf_token %}
            {{ form  }}
            <input type="submit" value="Log In">
        </form>
    {% endblock %}
    ```

#### Logging User In After Signing Up

Inside the accounts/views.py, lets finish what we started. We get the user who has filled the form and try to log them in. 

```python
from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login

def signup_view(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect('articles:list')
    else:
        form = UserCreationForm()
    return render(request, 'accounts/signup.html', {'form':form})

def login_view(request):
    if request.method =="POST":
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            return redirect('articles:list')
    else:
        form = AuthenticationForm()
    return render(request, 'accounts/login.html', {'form':form})
```
Pay attention to how different the way we use to retrieve the user when we logging in and signing up

#### Logging Out User

* Add a new url for logging out on urls.py
    ```python
    path('logout/', views.logout_view, name="logout"),
    ```

* Make a new view on views.py
    ```python
    def logout_view(request):
        if request.method == "POST":
            logout(request)
            return redirect('articles:list')
    ```

* Create a new html template for log out button, so create it inside the base_layout.html:

    ```html
    {% load static from staticfiles %}

    <!DOCTYPE html>
    <html>

    <head>
        <meta charset="utf-8">
        <title>Articles</title>
        <link rel="stylesheet" href="{% static 'styles.css' %}">
    </head>

    <body>
        <div class="wrapper">
            <h1><a href="{% url 'articles:list' %}"><img src="{% static 'logo.png' %}" alt="logo" height="42" width="42"/></a></h1>
            <nav>
                <ul>
                    <li>
                        <form class="logout-link" action="{% url'accounts:logout' %}" method="POST">
                            {% csrf_token %}
                            <button type="submit">Log Out</button>
                        </form>
                    </li>
                </ul>
            </nav>
            {% block content %}
            {% endblock %}
        </div>
    </body>

    </html>
    ```

### Accounts and Articles Apps Together

#### Requiring Login + Create New Article

* Create a new Article url for create (put it before the slug)
    ```python
    path('create/', views.artcle_create, name="create"),
    ```

* Create a new view:
    ```python
    def article_create(request):
        return render(request, 'articles/article_create.html')
    ```

* Create a new html template, we name it article_create.html:
    ```html
    {% extends 'base_layout.html' %}

    {% block content%}
    <div class="create-article">
        <h2>Create An Awesome New Article!</h2>
    </div>

    {% endblock %}
    ```

* To induce the login-required feature, we need to add a specific decorator to our function:
    ```python
    @login_required(login_url="/accounts/login")
    def article_create(request):
        return render(request, 'articles/article_create.html')
    ```
Now the website would force me to log in (if not logged in) before creating a new article

#### Redirecting After Logging In

We need to know where to redirect our users after they log in. Lets say a user wanted to create a new article before logging in. He would be redirected to our login form, but our login view would just redirect him to article_list view, which is not what he would have wanted. He wanted to create a new article, so there is the view we would like to redirect him to.  

We can get this *destination* variable from the login link, especially the **next** variable. We can get it as it is a GET request, and a GET request fortunately has that such attribute. In order to achieve this, we might need to tweak our login view as follows:  

```html
{% extends 'base_layout.html' %}

{% block content%}
    <h1>Log In</h1>
    <form class="site-form" action="{% url 'accounts:login' %}" method="POST">
        {% csrf_token %}
        {{ form  }}
        {% if request.GET.next %}
            <input type="hidden" name="next" value="{{ request.GET.next }}">
        {% endif %}
        <input type="submit" value="Log In">
    </form>
{% endblock %}
```

And then we could make a conditional in our login view:
```python
def login_view(request):
    if request.method =="POST":
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            if "next" in request.POST:
                return redirect(request.POST.get("next"))
            return redirect('articles:list')
    else:
        form = AuthenticationForm()
    return render(request, 'accounts/login.html', {'form':form})
```

Now it would redirect you to articles/create/ if you wanted to create an article in the first place, but it would redirect you to articles/ if you wanted to just login from the first place.


#### Model Form

##### Making Model Form

* In articles folder, create a forms.py

    ```python
    from django import forms
    from . import models

    class CreateArticle(forms.ModelForm):
        class Meta:
            model = models.Article
            fields = [
                "title",
                "body",
                "slug",
                "thumb"
            ]
    ```

* Make changes to the article_create view on articles/views.py:

    ```python
    from . import forms

    ...

    @login_required(login_url="/accounts/login")
    def article_create(request):
        form = forms.CreateArticle()
        return render(request, 'articles/article_create.html', {'form':form})
    ```

* On article_create.html, change into:
    ```html
    {% extends 'base_layout.html' %}

    {% block content%}
    <div class="create-article">
        <h2>Create An Awesome New Article!</h2>
        <form class="site-form" action="{% url 'articles:create' %}" method="POST" enctype="multipart/form-data">
            {% csrf_token %}
            {{ form }}
            <input type="submit" value="Create">
        </form>
    </div>

    {% endblock %}
    ```
the enctype is needed everytime we had an upload field (the thumbnail in this case)

* We need to reupdate the view again because we now have an upload field, on views.py:

    ```python
    @login_required(login_url="/accounts/login")
    def article_create(request):
        if request.method == "POST":
            form = forms.CreateArticle(request.POST, request.FILES)
            if form.is_valid():
                #save article to database
                return redirect('articles:list')
        else:
            form = forms.CreateArticle()
        return render(request, 'articles/article_create.html', {'form':form})
    ```

#### Associate Articles with Users

* Add another field on Article Model, which is User, as shown:

    *Note:* All of our currently made articles don't have users, so we must delete all of them to ensure that nothing could go wrong with it. Go to admin page and delete them all before proceeding.

    ```python
    from django.db import models
    from django.contrib.auth.models import User

    class Article(models.Model):
        title = models.CharField(max_length=100)
        slug = models.SlugField(max_length=100)
        body = models.TextField()
        date = models.DateTimeField(auto_now_add=True)
        thumbnail = models.ImageField(default='default.png', blank=True)
        author = models.ForeignKey(User, default=None, on_delete=models.CASCADE)

        def __str__(self):
            return self.title

        def snippet(self):
            if len(self.body) > 100:
                return self.body[:100] + " ..."
            else:
                return self.body
    ```

* Again, migrate the changes on the database  

    ```shell
    python manage.py makemigrations && python manage.py migrate
    ```

* Get the author in the front-end side, on article/views.py, write:

    ```python
    @login_required(login_url="/accounts/login")
    def article_create(request):
        if request.method == "POST":
            form = forms.CreateArticle(request.POST, request.FILES)
            if form.is_valid():
                # Get the author, but not saving it just yet
                instance = form.save(commit=False)
                instance.author = request.user
                # Now save it for real
                instance.save()
                return redirect('articles:list')
        else:
            form = forms.CreateArticle()
        return render(request, 'articles/article_create.html', {'form':form})
    ```

#### Checking Login Status and Show Login/Logout Button Accordingly

We just need a simple if-else in our base_layout.html as shown:

```html
{% load static from staticfiles %}

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Articles</title>
    <link rel="stylesheet" href="{% static 'styles.css' %}">
</head>

<body>
    <div class="wrapper">
        <h1><a href="{% url 'articles:list' %}"><img src="{% static 'logo.png' %}" alt="logo" height="42" width="42"/></a></h1>
        <nav>
            <ul>
                {% if user.is_authenticated %}
                <li>
                    <form class="logout-link" action="{% url 'accounts:logout' %}" method="POST">
                        {% csrf_token %}
                        <button type="submit">Log Out</button>
                    </form>
                </li>
                <li><a href="{% url 'articles:create' %}" class="highlight">New Article</a></li>
                {% else %}
                <li><a href="{% url 'accounts:login' %}">Log In</a></li>
                <li><a href="{% url 'accounts:signup' %}">Sign Up</a></li>
                {% endif %}
            </ul>
        </nav>
        {% block content %}
        {% endblock %}
    </div>
</body>

</html>
```

#### Redirecting Home Page (Index)

Lets change our mysite/urls.py so when we type the web link ONLY, it will redirect us to the articles' list. Import it then add a new path.

```python
from django.conf import settings
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import path, include
from django.conf.urls.static import static
from articles import views as article_views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('articles/', include('articles.urls')),
    path('accounts/', include('accounts.urls')),
    path('', article_views.article_list, name="home" )
]

urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
```

Next part is changing the url'article:list' in base_layout to url'home'.  

### Slugifying Articles' Title

Back to code, create a new file inside assets/ and name it slugify.js. We are going to use javascript here. Write the following

```java
const titleInput = document.querySelector('input[name=title]');
const slugInput = document.querySelector('input[name=slug]');

const slugify = (val) => {
    return val.toString().toLowerCase().trim()
        .replace(/&/g,'-and-')      //replace & with -and-
        .replace(/[\s\W-]+/g,'-')   //replace spaces, non alphabetical chars and dashes with a single dash
}

titleInput.addEventListener('keyup', (e) => {
    slugInput.setAttribute('value', slugify(titleInput.value));
});
// keyup event is the event produced when user lift his finger from a key
```

And then apply the script to your articles/article_create.html by adding the second-last line like so:

```html
{% extends 'base_layout.html' %}

{% block content%}
    <div class="create-article">
        <h2>Create An Awesome New Article!</h2>
        <form class="site-form" action="{% url 'articles:create' %}" method="POST" enctype="multipart/form-data">
            {% csrf_token %}
            {{ form }}
            <input type="submit" value="Create">
        </form>
    </div>
    <script src="/static/slugify.js"></script>
{% endblock %}
```

#### Adding Comments to Articles

Now we are going to make comments for our each articles. As you might have just thought, each article can have its own comments. It sounds like we need a relation between comments and articles using Foreign Key, just like between our authors and articles.

* Make the new Comment Model, in articles/models.py, add:  

    ```python
    class Comment(models.Model):
        article = models.ForeignKey('articles.Article', on_delete=models.CASCADE, related_name='comments')
        text = models.CharField(max_length=100)
        date = models.DateTimeField(auto_now_add=True)
        author = models.ForeignKey(User, default=None, on_delete=models.CASCADE)

        def __str__(self):
            return self.text
    ```

* Make the urls to create the comment, in articles/urls.py, add this into the urlpatterns:

    ```python
    path('<slug>/comment/', views.article_comment, name="comment"),
    ```

* Make the form to create new comments, in articles/forms.py, write this new class:  

    ```python
    class CreateComment(forms.ModelForm):
        class Meta:
            model = models.Comment
            fields = [
                "text",
            ]
            
    ```


* Make the html view of that form, inside articles/templates/articles, create a new html file named article_comment_create.html as so:

    ```html
    {% extends 'base_layout.html' %}

    {% block content%}
        <div class="create-comment">
            <h2>Write Your Comment Here!</h2>
            <form class="site-form" action="{% url 'articles:comment' slug=article.slug %}" method="POST">
                {% csrf_token %}
                {{ form }}
                <input type="submit" value="Create">
            </form>
        </div>
    {% endblock %}
    ```

* Make the view for commenting articles, inside articles/views.py:  

    ```python
    @login_required(login_url="/accounts/login")
    def article_comment(request, slug):
        article = Article.objects.get(slug=slug)
        if request.method == "POST":
            form = forms.CreateComment(request.POST)
            if form.is_valid():
                instance = form.save(commit=False)
                instance.author = request.user
                instance.article = article
                instance.save()
                return redirect('articles:detail', slug=slug)
        else:
            form = forms.CreateComment()
        return render(request, 'articles/article_comment_create.html', {'form': form, 'article':article})
    ```

* Display the comments in our article's detail page, on article_detail.html :  

```html
{% extends 'base_layout.html' %}

{% block content%}
    <div class="article_detail">
        <div class="article">
            <img src="{{article.thumbnail.url}}"/>
            <h2>{{ article.title }}</h2>
            <p>{{ article.body }}</p>
            <p>{{ article.date }}</p>
            <a class="button" href="{% url 'articles:comment' slug=article.slug %}">Add comment</a>
            {% for comment in article.comments.all %}
                <div class="comment">
                    <strong>{{ comment.author }}</strong>
                    <p>{{ comment.text|linebreaks }}</p>
                    <p>{{ comment.date }}</p>
                </div>
            {% empty %}
                <p>No comments here yet :(</p>
            {% endfor %}
        </div>
    </div>
    
{% endblock %}
```

#### Adding Likes to Articles

Just like we add the Comment Model, now we make a new Like Model. Do not forget its reference to our Article Model.

* Make the new Like Model, in articles/models.py, add:

    ```python
    class Like(models.Model):
        author = models.ForeignKey(User, default=None, on_delete=models.CASCADE)    
        article = models.ForeignKey('articles.Article', on_delete=models.CASCADE, related_name='likes')
        date = models.DateTimeField(auto_now_add=True)
    ```

* Next stop, we make the new urls, add this into the urlpatterns in articles/urls.py:  

    ```python
    path('<slug>/like/', views.article_like, name="like"),
    ```

* Now we need to show this Like button inside our articles' detail pages. Although it is practically a button, it serves as a POST form. Write this block of code inside the article_detail.html :  

    ```html
    {% extends 'base_layout.html' %}

    {% block content%}
        <div class="article_detail">
            <div class="article">
                <img src="{{article.thumbnail.url}}"/>
                <h2>{{ article.title }}</h2>
                <p>{{ article.body }}</p>
                <p>{{ article.date }}</p>
                <form id="likebutton" action="{% url 'articles:like' slug=article.slug%}" method="POST">
                    {% csrf_token %}
                    <button type="submit" value="Like">Like IT</button>
                </form>
                <a class="button" href="{% url 'articles:comment' slug=article.slug %}">Add comment</a>
                {% for comment in article.comments.all %}
                    <div class="comment">
                        <strong>{{ comment.author }}</strong>
                        <p>{{ comment.text|linebreaks }}</p>
                        <p>{{ comment.date }}</p>
                    </div>
                {% empty %}
                    <p>No comments here yet :(</p>
                {% endfor %}
            </div>
        </div>
        
    {% endblock %}
    ```
 
 * After having the form made, we need to handle the view, so inside views.py:  

    ```python
    @login_required(login_url="/accounts/login")
    def article_like(request, slug):
        article = Article.objects.get(slug=slug)
        if request.method == "POST":
            new_like, created = Like.objects.get_or_create(author=request.user, article=article)
            if created:
                new_like.save()   
                return redirect('articles:detail', slug=slug)
        return redirect(request.META['HTTP_REFERER'])
    ```

    The HTTP_REFERER means that it would redirect to the same previous page. The 'if created' part of the conditional is set to work if only the user has not liked the article before. This way we will limit the like for each users to just once per article (the users cannot spam the like button to increase the likes of the article he chose).

#### Displaying Likes and Comments Count on Articles

Lastly, we could display the numbers of likes and comments by adding some lines to our html like :

```html
<h6>{{ article.likes.all.count }} Likes</h6>
<h6>{{ article.comments.all.count }} Comments</h6>
```

* On article_detail.html :

    ```html
    {% extends 'base_layout.html' %}

    {% block content%}
        <div class="article_detail">
            <div class="article">
                <img src="{{article.thumbnail.url}}"/>
                <h2>{{ article.title }}</h2>
                <h6>{{ article.likes.all.count }} Likes</h6>
                <h6>{{ article.comments.all.count }} Comments</h6>
                <p>{{ article.body }}</p>
                <p>{{ article.date }}</p>
                <form id="likebutton" action="{% url 'articles:like' slug=article.slug%}" method="POST">
                    {% csrf_token %}
                    <button type="submit" value="Like">Like IT</button>
                </form>
                <a class="button" href="{% url 'articles:comment' slug=article.slug %}">Add comment</a>
                {% for comment in article.comments.all %}
                    <div class="comment">
                        <strong>{{ comment.author }}</strong>
                        <p>{{ comment.text|linebreaks }}</p>
                        <p>{{ comment.date }}</p>
                    </div>
                {% empty %}
                    <p>No comments here yet :(</p>
                {% endfor %}
            </div>
        </div>
        
    {% endblock %}
    ```

* On article_list.html:  

    ```html
    {% extends 'base_layout.html' %}

    {% block content%}
        <h1>Article List</h1>
        <div class="articles">
            {% for article in articles %}
                <div class="article">
                    <h2><a href="{% url 'articles:detail' slug=article.slug %}">{{ article.title }}</a></h2>
                    <h6>{{ article.likes.all.count }} Likes</h6>
                    <h6>{{ article.comments.all.count }} Comments</h6>
                    <p>{{ article.snippet }}</p>     
                    <p>{{ article.date }}</p>     
                    <p class="author">added by: {{ article.author.username }}</p>
                </div>
            {% endfor %}
        </div>
    {% endblock %}
    ```

### Styling (Optional Aesthetics)

~*Modifying the styles.css*~

## Congratulations!

Now you have it, your own Django app! Thanks and Have Fun now~

## Author

* **Jonathan Christopher Jakub** - [Jonathanjojo](https://gitlab.com/Jonathanjojo)

## Acknowledgement

* [BEM CSUI 2017](https://bem.cs.ui.ac.id/)
* [The Net Ninja](https://www.youtube.com/channel/UCW5YeuERMmlnqo4oq8vwUpg) 