const titleInput = document.querySelector('input[name=title]');
const slugInput = document.querySelector('input[name=slug]');

const slugify = (val) => {
    return val.toString().toLowerCase().trim()
        .replace(/&/g,'-and-')      //replace & with -and-
        .replace(/[\s\W-]+/g,'-')   //replace spaces, non alphabetical chars and dashes with a single dash
}

titleInput.addEventListener('keyup', (e) => {
    slugInput.setAttribute('value', slugify(titleInput.value));
});
// keyup event is the event produced when user lift his finger from a key